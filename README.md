# Project - nrp 
### written by Chris McNabb  => Dec 1, 2013
 
=================================
 
# OVERALL DESCRIPTION
>
> ## New Ruby Program is a bash script to generate a new ruby program skeleton with the following features for consistency:
> - README.md - with template to get started quickly
> - Gemfile
> - Initialize Git repo
> - Creates a gemset
> - Creates .gitignore file with a few common values
> - Creates first git tag
> - Generates release version
> - Displays initial git log upon completion
 
# USAGE
>
> ### Run the following to create a new project:
>
     $ nrp new_project_name
>
> For example, 
>
     $ nrp my_cool_dragon_slayer_project
>
 
